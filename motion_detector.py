"""
PIR motion detector
"""

class MotionDetector:

    def __init__(self, led_pin, pir_output):
        self._led_pin = led_pin
        self._pir_output
        self._setup()
    
    def setup(self):
        try:    
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self._pir_output, GPIO.IN)
            GPIO.setup(self._led_pin, GPIO.OUT)
            print("Initialized PIR on pin: {self._pir_output}")
            print("Initialized LED on pin: {self._led_pin}")
        except Exception as err:
            print(f"Problem with GPIO initialization: {err}")


    def detect(self, sample_amount, interval_sec, error_threshold_percentage):
    '''sample batch and compare to allowed errors'''
        
        samples = []
        while len(samples) < sample_amount:
            samples.append(GPIO.input(PIR_trigger))
            time.sleep(interval_sec)
        result = sum(samples) / sample_amount
        if result > error_threshold_percentage:
        
            motion_detect_count += 1
            return True, result
        return False, result

    def set_led(self, state):
        if state:
            GPIO.output(self._led_pin, 1)
        else:
            GPIO.output(self._led_pin, 0)



