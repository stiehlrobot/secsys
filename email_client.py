import smtplib
import credut
import os
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
import email.mime.application
from email import encoders

"""Derive from this tutorial> https://bc-robotics.com/tutorials/sending-email-using-python-raspberry-pi/"""

SMTP_SERVER = 'smtp.gmail.com' #Email Server (don't change!)
SMTP_PORT = 587 #Server Port (don't change!)
USERNAME = credut.get_address()
PASSWORD = credut.get_pw()
RECIPIENT = credut.get_recipient()

class Email:
    """Class for email operations"""

    def send(self, msg):
        '''sends email with subject, content to recipient'''
        print("Starting to send..")
        try:
            with smtplib.SMTP(SMTP_SERVER, SMTP_PORT) as s:
                s.ehlo()
                s.starttls()
                s.ehlo()
                s.login(USERNAME, PASSWORD) 
                s.sendmail(USERNAME, RECIPIENT, msg)        
                s.close()
            print('Breach email sent')
        except:
            print("Unable to send the email. Error: ", sys.exc_info()[0])
            raise
    
    def create_email_msg(self):
        #Create the Message
        print("Attempting to create message..")
        msg = MIMEMultipart()
        msg['Subject'] = 'Junkfactory breached!'
        msg['From'] = USERNAME
        msg['To'] = RECIPIENT
        return msg

    def attach_video(self, outer, captured_filepath):

        print("Captured filepath: {}".format(captured_filepath))
        # Add the attachments to the message
        try:
            with open(captured_filepath, 'rb') as fp:
                print("creating mimebase")
                msg = MIMEBase('application', "octet-stream")
                print("adding payload as fp.read")
                msg.set_payload(fp.read())

            encoders.encode_base64(msg)
            print("Encoders encoded to b64")
            msg.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(captured_filepath))
            print("Content dispo header added!")
            outer.attach(msg)
            print("Attached to outer msg")
        except:
            print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise
        
        return outer.as_string()
 
        
