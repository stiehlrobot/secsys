"""
Main logic for Motion Sensing RaspiCam Security System
Features:
- PIR motion sensor
- Night vision video recoder
- Video packing and sending via email
"""
import RPi.GPIO as GPIO
import logging
import glob
import sys
import time
import picamera
from email.mime.multipart import MIMEMultipart
from subprocess import call
import os
from datetime import datetime
import email.mime.application
from email_client import Email
from datetime import datetime
import credut
from keypad import Keypad
from alarm import Alarm

PIR_trigger = 26 #set GPIO pin here
PIR_led = 19 #pir trigger led
motion_detect_count = 0

def print_and_log(msg: str, log_type: str):
    print(msg)
    if log_type == 'debug':
        logging.debug(msg)
    elif log_type == 'info':
        logging.info(msg)
    else:
        logging.info("Unrecognized log type, defaults to info: {}".format(msg))

def setup_logger():
    try:
        logging.basicConfig(filename='event.log', encoding='utf-8', level=logging.DEBUG)
        logging.debug('Started logging..')
    except:
        print("Problem with logging initialization")
        
def activate_security(keypad, alarm):
    #activate siren()
    '''starts security procedures'''   

    #keypad security
    keypad.reset_sequence()
    keypad.activate_input(30, 0.05)
    if os.getenv('secsys-keypass') != keypad.get_sequence():
        print_and_log("Incorrect passkey, activating alarm!")
        keypad.reset_sequence()
    else:
        return
  
    #surveillance camera recording
    record_name = record_video()
    converted_vid = convert_video(record_name)
    path_to_send = os.path.join(os.getcwd(), converted_vid)
    send_breach_email(path_to_send)

    #activate alarm for 600 seconds
    alarm.activate(600)
    print_and_log("Sleeping for 600 seconds..",'info')
    time.sleep(600)

def send_breach_email(fname):
    '''send notification of breach via email'''
    print_and_log("Sending breach email procedure commencing..", 'info')
    emailcli = Email()
    print_and_log("Authenticated email client",'info')

    msg = emailcli.create_email_msg()
    print_and_log("Email msg created", 'info')
    
    #attach video to msg
    msg_with_video = emailcli.attach_video(msg, fname)
    print_and_log("Email msg video attached", 'info')
    
    #send email with video attachment
    emailcli.send(msg_with_video)
    print_and_log("Email sent to administrator!", 'info')

def record_video():
    '''record video using raspivid'''
    try:
        record_name = datetime.now().strftime("%Y-%m-%dT%H-%M-%S.h264")
        call(["raspivid", "-t", "10000", "-o", record_name])
        print_and_log("Started recording video with name: {}".format(record_name), 'info')
        time.sleep(10)
    except Exception as err:
        print_and_log("Problem recording video: {}".format(err), 'debug')
    return record_name

def record_video_picamera():
    '''Record security footage'''
    try:
        camera = picamera.PiCamera()
        record_name = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
        full_record_name = record_name+'.h264'
        camera.resolution = (640, 480)
        print_and_log("Starting to record video with name {}".format(full_record_name), 'info')
        camera.start_recording(full_record_name)
        camera.wait_recording(5)
        camera.stop_recording()
    finally:
        camera.close()
        print_and_log("Video recording ended. Camera closed.", 'info')
    #https://picamera.readthedocs.io/en/release-1.13/recipes1.html
    return full_record_name

def convert_video(record_name):
    '''coverting video from .h264 to .mp4'''
    print_and_log("Record name: {}".format(record_name), 'info')
    new_name = "{}.mp4".format(record_name[:-5])
    cmds = "MP4Box -add {} {}".format(record_name, new_name)
    try:
        call([cmds], shell=True)
        print_and_log("video converted", 'info')
    except Exception as er:
        print_and_log("Problem converting video: {}".format(er), 'error')

    return new_name

def cleanup_old_videos():
    
    print_and_log("Housekeeping.. removing old video files:", 'info')
    for fl in glob.glob("*.mp4"):
        if fl:
             try:
                 os.remove(fl)
                 print_and_log("Removed mp4", 'info')
             except Exception as rem_error:
                 print_and_log(rem_error, "debug")
    for fil in glob.glob("*.h264"):
        if fil:
           try:
               os.remove(fil)
               print_and_log("Removed h264", 'info')
           except Exception as rem_error2:
               print_and_log(rem_error, 'debug')
            


def main():
    '''main'''
    setup_logger()

    keypad_pins = {1: 2,
              2: 3,
              3: 4,
              4: 17,
              5: 13,
              6: 22,
              7: 10,
              8: 9,
              9: 11}
    keypad = Keypad(keypad_pins)

    alarm = Alarm(5) #check GPIO for alarm relay control
    detector = MotionDetector(19, 26) #led, pir

    try:
        while True:
            #remove old files
            if int(time.time()) % 43200  == 0:

                cleanup_old_videos()

            time.sleep(0.1)
            
            PIR_result = detect_motion_PIR(10, 1.0, 0.8) #take 10 samples, 1.0 interval, 0.2 error rate
            if PIR_result[0]:
                print_and_log("Motion detected! Result : {}".format(PIR_result[1]), 'info')
                activate_security(keypad, alarm)
            else:
                print("No motion. Result {}".format(PIR_result[1]))
            time.sleep(0.1)

    except:
        print("Exception unknown:", sys.exc_info()[0])
        #GPIO.cleanup()
 
    
if __name__=='__main__':
    main()


