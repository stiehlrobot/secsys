"""
Class for controlling raspberry pi relay shield
"""

import RPi.GPIO as GPIO

class Relay:

    def __init__(self, gpio):
        self._control_pin = gpio
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._control_pin, GPIO.OUT)

    def set(state):
        if state:
            GPIO.output(self._control_pin, GPIO.HIGH)
        else:
            GPIO.output(self._control_pin, GPIO.LOW)

class RelayShield:

    def __init__(self, pins):
        self._relays = init_relays(pins)

    def init_relays(self, pins):
        relays = {}
        for num, pin enumerate(self._pins):
            self._relays[num] = Relay(pin)
        return relays

    def set(relay_number, state):
        try:
            self._relays[relay_number].set(state)
        except KeyError:
            print("No relay found with this number!")

class Alarm:

    def __init__(self, gpio_pin):
        self._relay = Relay(gpio_pin)
    
    def activate(self, time_duration_s):
        self._relay.set(1)
        t_start = time.time()
        while time.time() < t_start + time_duration_s:
            time.sleep(0.5)
        self._relay.set(0)
           
