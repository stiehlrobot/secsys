"""
Class for custom 9 key keypad
Initializtion needs 9 GPIO pin numbers
"""

from gpiozero import Button
import time

class Keypad:

    def __init__(self, pins):
        self._pins = pins
        self._buttons = {}
        self._sequence = []
        self._initialize_buttons()

    def _initialize_buttons(self):
        '''initialize all buttons'''
        try:
            for value, gpio_pin in self._pins.items():
                self._buttons[value] = Button(pin)
        except Exception as err:
            print("Problem initializing GPIO buttons")
            print(err)
            
    def reset_sequence(self):
        '''resets the pressed sequence'''
        self._sequence = []

    def activate_input(self, time_period_s, refresh_rate_ms):
        '''Actively listen to the input from the keypad for given time period and use refresh rate provided'''
        t_start = time.time()
        while time.time() < t_start + time_period:
            for val, b in self._buttons.items():
                if b.is_pressed():
                    self._sequence.append(val)
            time.sleep(refresh_rate_ms)

    def get_sequence(self):
        return self._sequence

def main():
    keypad_pins = {1: 2,
              2: 3,
              3: 4,
              4: 17,
              5: 13,
              6: 22,
              7: 10,
              8: 9,
              9: 11}
    
    kp = Keypad(keypad_pins)

    kp.activate_input(10, 0.05)
    seq_1 = kp.get_sequence()
    print(f"Keypad captured seqence: {seq_1}")
    
    kp.reset_sequence()
    seq_2 = kp.get_sequence()
    print(f"Keypad captured seqence: {seq_2}")
    kp.reset_sequence()


if __name__ == '__main__':
    main()



